#-*- coding: utf-8 -*-
from db import workdb

def workTracks(data,chanel_id,playlist_id):
    sql ='insert or replace into chanel_track (chanel_id,track_id,playlist_id,name_track,title,author,duration,url,checksum,volume,order_track)'
    sql = sql +' values(:chanel_id,:track_id,:playlist_id,:name_track,:title,:author,:duration,:url,:checksum,:volume,:order_track)'
    params={}
    for l in data:
        params['chanel_id'] = chanel_id
        params['playlist_id']  = playlist_id
        params['track_id'] = l['id']
        params['name_track'] = l['name']
        params['title'] = l['title']
        params['author'] = l['author']
        params['url'] = l['url']
        params['checksum'] = l['checksum']
        params['volume'] = l['volume']
        params['order_track'] = l['order']
        params['duration'] = l['duration']
        workdb.query(sql,params)
    pass


# обработаем и занесем в базу playlist'ы
def workPlaylist(data,chanel_id):
    sql='insert or replace into playlist (chanel_id,id,playlist_name,volume,block_size,playback_order,order_playlist,period_start,period_end) '
    sql=sql+' VALUES(:chanel_id,:id,:playlist_name,:volume,:block_size,:playback_order,:order_playlist,:period_start,:period_end)'
    params = {}
    for l in data :
        params['chanel_id'] = chanel_id
        params['id'] = l['id']
        params['playlist_name'] = l['name']
        params['volume'] = l['volume']
        params['block_size'] = l['block_size']
        params['playback_order'] = l['playbackOrder']
        params['order_playlist'] = l['order']
        if l['period'] == None :
            params['period_start'] = ''
            params['period_end'] = ''
        else :
            params['period_start'] = l['period']['start']
            params['period_end'] = l ['period']['end']
        print "SHEDULE\n"
        print l['schedule']
        workdb.query(sql,params)
        workTracks(l['tracks'],chanel_id,l['id'])
        pass
    workdb.commit()
    pass

# обработка данных для компании
def workCompany(data,chanel_id=None):

    pass

# загрузим трек в кеш
def loadInCacheTrack(path_cache,track_url):

    pass
