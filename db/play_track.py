#-*- coding: utf-*-
import pygst

import gtk

pygst.require('0.10')
import gst
import gobject
import os,thread,time

#https://habrahabr.ru/post/179167/
#https://brettviren.github.io/pygst-tutorial-org/pygst-tutorial.pdf
#https://github.com/hadware/gstreamer-python-player/blob/master/seek.py

class PlayerGT:
    play_thread_id = None

    callback = None

    def __init__(self,callback=None):
        self.callback = callback
        print gst.version_string()
        self.pl = gst.element_factory_make("playbin","player")
        pass

    def playUrl(self,url):
        #mainloop = gobject.MainLoop()
        self.pl.set_property('uri',url)

        print self.pl.get_state()

        bus = self.pl.get_bus()
        bus.add_signal_watch()
        #bus.connect("message",self.on_message,mainloop)
        bus.connect("message",self.on_message)

        self.pl.set_state(gst.STATE_PLAYING)
        state = self.pl.get_state()
        print state

        self.play_thread_id = thread.start_new_thread(self.play_thread, ())
        #mainloop.run()
        pass

    def stopPlay(self):
        pass

    def on_message(self,bus,message,loop=None):
        t = message.type
        print  t

        if t == gst.MESSAGE_EOS :
            self.play_thread_id = None
            self.pl.set_state(gst.STATE_PAUSED)
            self.pl.set_state(gst.STATE_NULL)
            state = self.pl.get_state()
            print state
            #loop.quit()
            self.callback(mode='exit')
        elif t == gst.MESSAGE_ERROR:
            err, debug = message.parse_error()
            self.pl.set_stateg(gst.STATE_NULL)
            self.play_thread_id = None
            print err,debug
            self.callback(mode='error')
        pass

    def play_thread(self):
        play_thread_id = self.play_thread_id
        gtk.gdk.threads_enter()
        print '00:00 / 00:00'
        gtk.gdk.threads_leave()
        while play_thread_id == self.play_thread_id:
            try:
                time.sleep(0.2)
                dur_int = self.pl.query_duration(gst.FORMAT_TIME)[0]
                print  'DUR :' + self.convert_ns(dur_int)
                self.convert_ns2(dur_int)
                if dur_int == -1:
                    continue
                dur_str = self.convert_ns(dur_int)
                gtk.gdk.threads_enter()
                print '00:00/ ' + dur_str
                self.callback('progress_init',self.convert_ns2(dur_int))
                gtk.gdk.threads_leave()
                break
            except Exception, e:
                print e
                pass

        time.sleep(0.2)
        while play_thread_id == self.play_thread_id:
            pos_int = self.pl.query_position(gst.FORMAT_TIME)[0]
            pos_str = self.convert_ns(pos_int)
            if play_thread_id == self.play_thread_id:
                gtk.gdk.threads_enter()
                #print pos_str + ' / ' + dur_str
                self.callback('progress',self.convert_ns2(pos_int),self.convert_ns2(dur_int))
                gtk.gdk.threads_leave()
            time.sleep(1)
            pass
        pass

    def convert_ns(self,t):
        s, ns = divmod(t, 1000000000)
        m, s = divmod(s, 60)
        if m < 60:
            return "%02i:%02i" % (m, s)
        else:
            h, m = divmod(m, 60)
            return "%i:%02i:%02i" % (h, m, s)

    def convert_ns2(self,t):
        s,ns = divmod(t,1000000000)
        print s,ns
        return s




def play_file (file):
    mainloop = gobject.MainLoop()
    pl = gst.element_factory_make("playbin", "player")
    pl.set_property('uri','file://'+os.path.abspath('11-here-to-stay.mp3'))
    pl.set_state(gst.STATE_PLAYING)
    mainloop.run()

