#-*- coding:utf-8 -*-
'''
    (c) CAV Inc
    Работа с конфигурацией и параметрами
    параметры сохраняются в каталоге пользователя
'''
import os
import ConfigParser
import uuid

prop={}

def TestAndGreateDir(foldername):
    ''' проверяет и создает при остутствии каталог '''
    if os.access(foldername,os.F_OK)!=True :
        os.mkdir(foldername)
        return True
    return False

def TestAndDefaultGreate():
    global prop
    user_path=os.path.expanduser('~')
    prop['USER PATH']=user_path
    if TestAndGreateDir(user_path+os.sep+'.cplayer'):
        SaveCFG(user_path+os.sep+'.cplayer'+os.sep+'player.ini')
        pass

def getKey(key):
    global prop
    return prop[key]

def setKey(key,val):
    global prop
    prop[key]=val
def testKey(key):
    global prop
    return prop.has_key(key)
def delKey(key):
    global prop
    del prop[key]

def LoadCFG(inifile):
    cp=ConfigParser.ConfigParser()
    cp.read(inifile)

    if cp.has_option("project","project_id"):
        setKey("project_id", cp.get('project', 'project_id'))
    else:
        setKey('project_id','12345')
    if cp.has_option('project','code_player'):
        setKey('code_player', cp.get('project', 'code_player'))
    else :
        setKey('code_player','a54fhvh7')

    if cp.has_option('project','uuid'):
        setKey('uuid',cp.get('project','uuid'))
    else :
        setKey('uuid',str(uuid.uuid1()).replace('-', ''))

    if cp.has_option('urls','web_url'):
        setKey('web_url',cp.get('urls','web_url'))



def SaveCFG(inifile):
    cp=ConfigParser.ConfigParser()
    cp.add_section('project')
    if testKey('project_id'):
        cp.set('project','project_id',getKey("project_id"))

    if testKey('code_player'):
        cp.set('project','code_player',getKey('code_player'))

    if testKey('uuid'):
        cp.set('project','uuid',getKey('uuid'))


    cp.add_section('urls')
    if testKey('web_url'):
        cp.set('urls','web_url',getKey('web_url'))

    if testKey('url_update'):
        cp.set('urls','url_update',getKey('url_update'))

    if testKey('cache_file'):
        cp.set('urls','cache_file',getKey('cache_file'))

    cp.write(open(inifile, 'w'))