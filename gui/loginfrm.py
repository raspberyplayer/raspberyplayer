#-*- coding: utf-8 -*-
import pygtk
import glu
from net.request import Request

pygtk.require('2.0')
import gtk


#http://pygtk.rosix.ru/%D1%83%D1%87%D0%B5%D0%B1%D0%BD%D0%B8%D0%BA-pygtk-2-0/%D0%9A%D0%BE%D0%BC%D0%BF%D0%BE%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0-%D0%B2%D0%B8%D0%B4%D0%B6%D0%B5%D1%82%D0%BE%D0%B2-gtk-hbox-gtk-vbox/%D0%A0%D0%B0%D0%B7%D0%BC%D0%B5%D1%89%D0%B5%D0%BD%D0%B8%D0%B5-%D1%82%D0%B0%D0%B1%D0%BB%D0%B8%D1%86%D0%B5%D0%B9/
#http://proft.me/2010/09/23/pygtk-sqlalchemy-pishem-katalogizator-filmov/
#https://eax.me/python-gtk/
#http://pygtk.rosix.ru/%D1%83%D1%87%D0%B5%D0%B1%D0%BD%D0%B8%D0%BA-pygtk-2-0/%D0%9D%D0%B0%D1%87%D0%B0%D0%BB%D0%BE-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%8B/

class LoginForm(gtk.Frame):
    callback = None

    def __set_icon(self, fname):
        iconw = gtk.Image()
        iconw.set_from_file(fname)
        return iconw

    def __init__(self,callback = None):
        gtk.Frame.__init__(self)
        self.callback = callback
        vbox=gtk.VBox()
        icon_box = gtk.HBox()

        settingButton = gtk.ToolButton(self.__set_icon("./img/setting.png"))
        settingButton.connect('clicked',self.__on_click,'setting_buttn')

        icon_box.pack_end(settingButton,False,False)

        vbox.pack_start(icon_box,False,False)

        table = gtk.Table(rows=2, columns=2, homogeneous=False)

        lb1 = gtk.Label('Проект')
        self.project = gtk.Entry()

        lb2 = gtk.Label('Код плеера')
        self.code_player = gtk.Entry()

        table.attach(lb1,0,1,0,1,ypadding=6)
        table.attach(self.project,1,2,0,1,ypadding=6)
        table.attach(lb2,0,1,1,2)
        table.attach(self.code_player,1,2,1,2)
        table.set_row_spacings(10)

        loginBt = gtk.Button('Вход')
        loginBt.connect('clicked',self.__on_click,'enter')

        hbox2 = gtk.HBox()
        version = gtk.Label("1.0.0.0")
        update_button = gtk.Button("Проверить обновления")
        update_button.connect('clicked',self.__on_click,'update')
        hbox2.pack_start(version,False,False)
        hbox2.pack_start(update_button,False,False)


        vbox.pack_start(table,False,False,4)
        vbox.pack_start(loginBt,False,False,4)

        vbox.pack_start(hbox2,False,False)

        self.setStoreValue()

        self.add(vbox)
        self.show_all()
        pass



    def __on_click(self,widget,data=None):
        if data == 'update':
            pass
        if data == 'enter':
            if len(self.project.get_text()) != 0:
                glu.setKey("project_id",self.project.get_text())

            if len(self.code_player.get_text()) != 0:
                glu.setKey("code_player",self.code_player.get_text())


            # регестрируемся на сервере
            net_rs = Request("http://saas.handh.ru/api/")
            result,data = net_rs.getSecyreKey(glu.getKey('project_id'),glu.getKey('code_player'),'TEST_RASPBERRY',glu.getKey('uuid'))
            if result == None :
                # по идее он должен сидет в цикле пока не изменистя состояние
                print 'Error',result,data
                return
            if result != 0 :
                print  'Error',result,data
                return False
            glu.setKey('token',data)
            print data


            if self.callback != None:
                self.callback(1)
            pass
        if data == 'setting_buttn':
            if self.callback != None :
                self.callback(2)
            pass
        pass

    def setStoreValue(self):
        if glu.testKey("project_id"):
            self.project.set_text(glu.getKey("project_id"))
        if glu.testKey("code_player"):
            self.code_player.set_text(glu.getKey("code_player"))

        pass

