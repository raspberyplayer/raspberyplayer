#-*- coding:utf-8 -*-
import pygtk

import glu
from db import workdb
from db import datawork
from db.play_track import PlayerGT
from net.request import Request

pygtk.require('2.0')
import gtk
import pprint

class MainFrm(gtk.Frame):

    company = {}

    track = []

    def __init__(self,callback = None):
        gtk.Frame.__init__(self)
        vbox = gtk.VBox()
        hbox = gtk.HBox()

        lb_prj = gtk.Label("Проект 1")
        hbox.pack_start(lb_prj,False,False)

        self.ch_cb = gtk.combo = gtk.ComboBox(model=None)
        self.ch_cb.connect("changed", self.on_ch_cb_changed)

        hbox.pack_start(self.ch_cb,False,False)

        lb_status = gtk.Label("Онлайн")
        hbox.pack_start(lb_status,False,False)

        rb_stereo = gtk.RadioButton(None, "Стерео")
        hbox.pack_start(rb_stereo,False,False)

        rb_mono = gtk.RadioButton(rb_stereo,"Моно")
        hbox.pack_start(rb_mono,False,False)

        vbox.pack_start(hbox,False,False)

        hbox_bt = gtk.HBox()
        exit_button = gtk.Button("Выход")
        exit_button.connect("clicked",self.__on_click,"exit")

        hbox_bt.pack_start(exit_button,False,True)


        vbox.pack_start(hbox_bt,False,False)

        self.lb_mess = gtk.Label("Загрузка контента")
        self.lb_Track = gtk.Label("Трек")
        self.lb_progress = gtk.ProgressBar()

        vbox.pack_start(self.lb_mess,False,False)
        vbox.pack_start(self.lb_Track,False,False)
        vbox.pack_start(self.lb_progress,False,False)


        self.add(vbox)
        self.show_all()

        #self.getCanel(); # запрос каналов
        self.setComboBox()
        
        pass

    def __on_click(self,widget,data=None):
        if data == 'exit':
            if self.callback != None:
                self.callback(0)
            pass
        pass

    def getCanel(self):
        net_rs = Request("http://saas.handh.ru/api/")
        res,data = net_rs.getChanal(glu.getKey('token'))

        #insert or replace into tbl2 (uid, recid, voice) values (:uid, :recid, :voice)
        #data = {}
        #data["uid"] = 1
        #data['recid'] = 1
        #data['voice'] = 1

        #cursor.execute(sql, data)

        #data = {}
        #data["uid"] = 1
        #data['recid'] = 1
        #data['voice'] = 2

        #cursor.execute(sql, data)

        #conn.commit()
        #conn.close()

        if res !=0 :
            return False

        sql ='insert or replace into chanels (id,name_chanel,reboot,start_time,end_time) values (:id,:name_chanal,:reboot,:start_time,:end_time)'
        params = {}
        for l in data:
            print l
            params['id'] = l['id']
            params['name_chanal'] = l['name'].decode('utf-8')
            params['reboot'] = l['reboot']
            params['start_time']=l['workTime']['startTime']
            params['end_time']=l['workTime']['endTime']
            workdb.query(sql,params)

            pass
        workdb.commit()
        
    def setComboBox(self):
        name_store = gtk.ListStore(int, str)
        cell = gtk.CellRendererText()
        sql = 'select id,name_chanel from chanels order by id;'
        result = workdb.queryResult(sql)
        for l in result:
            name_store.append([l[0], l[1]])
        self.ch_cb.pack_start(cell, True)
        self.ch_cb.add_attribute(cell, 'text', 1)
        self.ch_cb.set_model(name_store)
        # добавить установку первого из загруженных в модели

        pass
        
    def on_ch_cb_changed(self, combo):
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            row_id= model[tree_iter][0]
            
            net_rs = Request("http://saas.handh.ru/api/")
            res,data = net_rs.getReestrChanal(row_id,glu.getKey('token'))
            print type(data)
            params = {}
            for l in data :
                #pprint.pprint(l)
                params['id'] = l ['id']
                params['name']  = l['name']
                params['published'] = l['published']
                params['priority'] = l ['priority']
                params['companyRelationship'] = l['companyRelationship']
                params['playback_order'] = l['playbackOrder']
                params['playback_mode'] = l['mode']
                params['block_info_count_blocks'] = l['blockInfo']['countBlocks']
                params['block_info_time'] = l['blockInfo']['time']
                params['block_info_timeType'] = l['blockInfo']['timeType']

                params['afterplayMode'] = l['afterplayMode']
                #params['weekInfoDisabled'] = l['weekInfoDisabled']
                params['weekInfo'] = l['weekInfo']
                params['concreteDates']  = l['concreteDates']
                params['period'] = l['period']
                self.company[l['id']] = params
                #print l['playlists']
                datawork.workPlaylist(l['playlists'],row_id)


                #pprint.pprint(params)
            #pprint.pprint(self.company)
            self.startPlayList(row_id)
        else :
            return False
        return True
        pass

    # запускаем обработку плей листов
    def startPlayList(self,chanel):
        self.pt = PlayerGT(self.callback_player)

        sql = 'select id from playlist where chanel_id='+str(chanel)
        res = workdb.queryResult(sql)
        for index in res:
            print index[0]
            sql = 'select url,title,author from chanel_track '
            sql = sql + 'where chanel_id='+str(chanel)+' and playlist_id='+str(index[0])
            sql = sql + ' order by order_track'
            track = workdb.queryResult(sql)
            for tr in track:
                print tr
                #self.lb_Track.set_text(tr[1])
                #pt.playUrl(tr[0])
                #
                self.track.append(tr)
                #break
            self.track.reverse()
            tr = self.track.pop()
            self.lb_Track.set_text(tr[1])
            self.pt.playUrl(tr[0])
        pass

    def nextTrackInPlayList(self):
        if len(self.track) != 0:
            tr = self.track.pop()
            self.lb_Track.set_text(tr[1])
            print tr
            self.pt.playUrl(tr[0])
        pass

    # обратная функция взаимодействия с ui
    def callback_player(self,mode,*args):
        if mode == 'exit':
            print 'Exit'
            self.nextTrackInPlayList()
        if mode == 'progress':
            print 'Progress'
            p = args[0]
            d = args[1]
            gtk.gdk.threads_enter()
            self.lb_progress.set_fraction(p/d)
            gtk.gdk.threads_leave()
        pass
    pass

