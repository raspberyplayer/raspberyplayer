#-*- coding: utf-8 -*-
import os
import pygtk

pygtk.require('2.0')
import gtk
import glu

class SettingFrm(gtk.Frame):
    callback = None

    def __set_icon(self, fname):
        iconw = gtk.Image()
        iconw.set_from_file(fname)
        return iconw

    def __init__(self,callback = None):
        gtk.Frame.__init__(self)
        self.callback = callback
        vbox = gtk.VBox()

        hbox = gtk.HBox()

        backButton = gtk.ToolButton(self.__set_icon("./img/back.png"))
        backButton.connect('clicked',self.__on_click,'back_button')

        hbox.pack_start(backButton,False,False)

        vbox.pack_start(hbox,False,True)

        table = gtk.Table(rows=3, columns=2, homogeneous=False)
        lb1 = gtk.Label("Веб-сервис")
        lb2 = gtk.Label("Url для обновления")
        lb3 = gtk.Label("Кэш-файлов")

        self.webUrl = gtk.Entry()
        self.urlUpdate = gtk.Entry()
        self.cacheFile = gtk.Entry()

        table.attach(lb1,0,1,0,1)
        table.attach(self.webUrl,1,2,0,1)
        table.attach(lb2,0,1,1,2)
        table.attach(self.urlUpdate,1,2,1,2)
        table.attach(lb3,0,1,2,3)
        table.attach(self.cacheFile,1,2,2,3)
        table.set_row_spacings(10)

        vbox.pack_start(table,True,False)

        saveButton = gtk.Button("Сохранить")
        saveButton.connect('clicked',self.__on_click,'savebutton')

        vbox.pack_start(saveButton,False,False)

        self.setStoreValue()

        self.add(vbox)
        self.show_all()

    def __on_click(self,widget,data=None):
        if data == 'savebutton':
            glu.setKey("web_url",self.webUrl.get_text())
            glu.setKey('url_update',self.urlUpdate.get_text())
            glu.setKey('cache_file',self.cacheFile.get_text())

            glu.SaveCFG(glu.getKey('USER PATH')+os.sep+'.cplayer'+os.sep+'player.ini')
            pass
        if data == 'back_button':
            if self.callback != None:
                self.callback(0)
                pass
                #self.callback(LoginForm(self.setPanel))

            pass
        pass

    def setStoreValue(self):
        if glu.testKey("web_url"):
            self.webUrl.set_text(glu.getKey("web_url"))
        if glu.testKey("url_update"):
            self.urlUpdate.set_text(glu.getKey("url_update"))
        if glu.testKey("cache_file"):
            self.cacheFile.set_text(glu.getKey("cache_file"))
    pass






