#-*- coding: utf-8 -*-
import pygtk

import glu
from gui.settingfrm import SettingFrm
from gui.loginfrm import LoginForm
from gui.mainfrm import MainFrm

pygtk.require('2.0')
import gtk
import os

class StartForm:
    win_list = []
    win_all = []

    def __destroy(self, widget):
        glu.SaveCFG(glu.getKey('USER PATH')+os.sep+'.cplayer'+os.sep+'player.ini')
        gtk.main_quit()
        return False

    def __init__(self):
        win = gtk.Window(gtk.WINDOW_TOPLEVEL)
        win.set_title('Player')
        win.set_default_size(win.get_screen().get_width(),400)
        win.connect('destroy',self.__destroy)
        self.vbox = gtk.VBox()

        #self.setPanel(SettingFrm())
        self.setPanel(LoginForm(self.changePanel))

        win.add(self.vbox)
        win.show_all()
        pass

    def setPanel(self,panel):
        if len(self.win_list) > 0:
            self.vbox.remove(self.win_list.pop())
        sl = panel
        self.win_list.append(sl)
        self.vbox.add(sl)
        pass

    def changePanel(self,id_panel):
        if id_panel == 0:
            self.setPanel(LoginForm(self.changePanel))
        if id_panel == 1:
            self.setPanel(MainFrm(self.changePanel))
        if id_panel == 2:
            self.setPanel(SettingFrm(self.changePanel))

        pass

    '''
    стартуем форму
    '''
    def main(self):
        gtk.main()
