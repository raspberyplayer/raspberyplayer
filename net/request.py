#-*- coding: utf-8 -*-
import requests
import json

#ttps://khashtamov.com/ru/python-requests/
#https://rtfm.co.ua/python-modul-requests/

class Request:
    BASE_URL=''
    version = ''
    cookei = {}

    def __init__(self,base_url):
        self.BASE_URL = base_url
        print requests.__version__
        version = requests.__version__
        pass

    # получние ключа безопасности
    def getSecyreKey(self,id_project,code_device,name_device,guid):
        #get_str = 'account/signin/?id={id проекта}&code={код устройства}&name={имя устройства}&guid={уникальный идентификатор устройства}'
        get_str = 'account/signin/?id='+id_project+'&code='+code_device+'&name='+name_device+'&guid='+guid+''
        result = requests.get(self.BASE_URL + get_str)
        if result.status_code != 200:
            return None,result.status_code
        jsx = json.loads(result.content)
        if jsx['errorCode'] != 0 :
            return jsx['errorCode'],jsx['errorMessage']
        #print jsx
        self.cookei = {'uid', jsx['data']['token']}
        return jsx['errorCode'],jsx['data']['token']
        # пример cookie = {'enwiki_session': '17ab96bd8ffbe8ca58a78657a918558'}
        #self.cookei = {'uid','ключь безопасности'}
        pass

    # получение каналов
    def getChanal(self,token):
        get_str = 'channel/getchannels/'
        # оптравляем запрос с куками
        self.cookei = {'uid': token}
        result = requests.get(self.BASE_URL + get_str,cookies=self.cookei)
        if result.status_code != 200:
            return None,None

        print result.headers

        jsx = eval(result.content)
        print jsx
        if (jsx['errorCode'] != 0):
            return jsx['errorCode'],jsx['errorMessage']
        data = jsx['data']
        for l in data:
            print l

        return jsx['errorCode'],data
        pass

    # получение расписания канала
    def getReestrChanal(self,chanal_id,token):
        get_str = 'campaign/getschedule/?channel='+str(chanal_id)
        self.cookei = {'uid':token}
        result = requests.get(self.BASE_URL + get_str,cookies=self.cookei)
        if result.status_code != 200:
            return None,None        
            
        #jsx = eval(result.content)
        jsx = json.loads(result.content)
        print jsx
        if (jsx['errorCode'] != 0):
            return jsx['errorCode'],jsx['errorMessage']        
        
        return jsx['errorCode'],jsx['data']
        pass

    # запрос на то изменилось ли расписание
    def getSheduleChanal(self,token):
        get_str = 'campaign/getschedulelastmodified/?channel=<идентификатор канала>'
        pass

    # Запрос на валидацию кода устройства для выхода из экрана блокировки.
    def getCheckAccount(self,code_device):
        get_str = 'account/check/?code={код устройства}'

    # загрузка трека
    def getTrack(self,track_url,token):
        result = requests.get(self.BASE_URL + track_url)

        pass

    # тестовый запрос о получения всех данных о устройсвах и проектах
    def getTest(self):
        result = requests.get(self.BASE_URL+'test/devices/')
        print result
        print result.status_code
        print result.headers
        print '---------------------------'
        print result.content
        print type(result.content)
        jsx = json.loads(result.content)
        print '---------------------------'
        print jsx
        
        js_str = result.json()
        for keys, value in js_str.items():
            print keys, value

        if js_str['errorCode'] != 0 :
            pass

        l = js_str['data']
        print l
        return 0,l

