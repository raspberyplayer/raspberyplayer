#!/usr/bin/python
#-*- coding: utf-8 -*-
import os

import gobject

import glu
from db import workdb

from gui.startfrm import StartForm

if __name__=='__main__':
    gobject.threads_init()
    workdb.openDb()
    glu.TestAndDefaultGreate()
    glu.LoadCFG(glu.getKey('USER PATH')+os.sep+'.cplayer'+os.sep+'player.ini')
    StartForm().main()


